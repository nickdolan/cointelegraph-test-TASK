"use strict";

class PaginationHelper {
  constructor(collection, itemsPerPage) {
    this.items = collection;
    this.itemsLength = collection.length;
    this.itemsPerPage = itemsPerPage;
  }

  itemCount() {
    return this.itemsLength;
  }

  pageCount() {
    return Math.ceil(this.items.length / this.itemsPerPage);
  }

  pageItemCount(pageIndex) {
    if (pageIndex < 0 || pageIndex >= this.pageCount()) {
      return -1;
    } else if (pageIndex === this.pageCount() - 1) {
      return this.itemCount() % this.itemsPerPage;
    }
    return this.itemsPerPage;
  }

  pageIndex(itemIndex) {
    return this.items[itemIndex] ? Math.floor(itemIndex / this.itemsPerPage) : -1;
  }
}

const should = require('chai').should();
const helper = new PaginationHelper(['a','b','c','d','e','f'], 4);

helper.pageCount().should.have.equal(2);
helper.itemCount().should.have.equal(6);
helper.pageItemCount(0).should.have.equal(4);
helper.pageItemCount(1).should.have.equal(2);
helper.pageItemCount(2).should.have.equal(-1);

helper.pageIndex(5).should.have.equal(1);
helper.pageIndex(2).should.have.equal(0);
helper.pageIndex(20).should.have.equal(-1);
helper.pageIndex(-10).should.have.equal(-1);