function PaginationHelper(collection, itemsPerPage) {
  this.items = collection;
  this.itemsLength = collection.length;
  this.itemsPerPage = itemsPerPage;
}

// Count of items on page.
PaginationHelper.prototype.itemCount = function() {
  return this.itemsLength;
};

// Count of pages for entire collection of items.
PaginationHelper.prototype.pageCount = function() {
  return Math.ceil(this.items.length / this.itemsPerPage);
};

// Items count on current page.
PaginationHelper.prototype.pageItemCount = function(pageIndex) {
  if (pageIndex < 0 || pageIndex >= this.pageCount()) {
    return -1;
  } else if (pageIndex === this.pageCount() - 1) {
    return this.itemCount() % this.itemsPerPage;
  }
  return this.itemsPerPage;
};

// Page number where item is located.
PaginationHelper.prototype.pageIndex = function(itemIndex) {
  return this.items[itemIndex] ? Math.floor(itemIndex / this.itemsPerPage) : -1;  
};

var should = require('chai').should();
var helper = new PaginationHelper(['a','b','c','d','e','f'], 4);

helper.pageCount().should.have.equal(2);
helper.itemCount().should.have.equal(6);
helper.pageItemCount(0).should.have.equal(4);
helper.pageItemCount(1).should.have.equal(2);
helper.pageItemCount(2).should.have.equal(-1);

helper.pageIndex(5).should.have.equal(1);
helper.pageIndex(2).should.have.equal(0);
helper.pageIndex(20).should.have.equal(-1);
helper.pageIndex(-10).should.have.equal(-1);